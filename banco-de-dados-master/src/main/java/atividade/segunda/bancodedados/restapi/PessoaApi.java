/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atividade.segunda.bancodedados.restapi;

import atividade.segunda.bancodedados.dao.PessoaDAO;
import java.util.List;
import atividade.segunda.bancodedados.domain.Pessoa;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author WilliamFernandoMende
 */
@RestController
public class PessoaApi {

    private PessoaDAO pessoaDAO = new PessoaDAO();

    @GetMapping("/pessoas")
    public List<Pessoa> obterPessoas() {
        return pessoaDAO.findAll();
    }

    //Trás a pessoa pelo ID digitado
    private PessoaDAO pessoaPeloID = new PessoaDAO();
    private Pessoa pessoa = new Pessoa();
    @RequestMapping(value = "id/{numero}", method = RequestMethod.GET)
    public Pessoa obterPeloID(@PathVariable(value = "numero") Integer numero) throws Exception {
        return (Pessoa) pessoaPeloID.peloID(numero);
    }

    //Trás as pessoas com o nome digitado
    private PessoaDAO pessoaPeloNome = new PessoaDAO();
    @RequestMapping(value = "nome/{primeiroNome}", method = RequestMethod.GET)
    public List<Pessoa> obterPeloNome(@PathVariable(value = "primeiroNome") String primeiroNome) throws Exception {
        primeiroNome = StringUtils.capitalize(primeiroNome);
        return pessoaPeloNome.peloNome(primeiroNome);
    }

    //Trás as pessoas que nasceram após o ano digitado
    private PessoaDAO pessoaPeloAno = new PessoaDAO();
    @RequestMapping(value = "ano/{anoNascimento}", method = RequestMethod.GET)
    public List<Pessoa> obterPeloAno(@PathVariable(value = "anoNascimento") int anoNascimento) throws Exception {
        return pessoaPeloAno.peloAno(anoNascimento);
    }
}



