/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  WilliamFernandoMende
 * Created: Nov 5, 2019
 */

CREATE TABLE Pessoa
(
Codigo INT NOT NULL AUTO_INCREMENT,
Nome VARCHAR (60) NOT NULL,
Data_Nascimento DATE,
Telefone CHAR (8),
Documento CHAR (14),
PRIMARY KEY (Codigo) );